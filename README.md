# Espejo Inteligentehttps://hackaday.io/project/159858-gesture-controlled-smart-mirror#menu-description
https://hackaday.io/project/162994-smart-mirror-with-google-assistant

Nuestra idea es hacer un espejo a partir de una pantalla de pc, en el cual se muetre información obtenida de internet de una cuenta de google como por ejemplo:
- Calendario.
- Clima.
- Agenda del día.

De lograr esto como base, el siguiente paso sería agregar la función de reconocimiento de comandos de voz para Google Assistant y funciones como la de reproducir música y un reloj despertador.

Proyectos de referencia.

https://hackaday.io/project/159858-gesture-controlled-smart-mirror#menu-description
https://hackaday.io/project/162994-smart-mirror-with-google-assistant
https://hackaday.io/project/6184-piclock-a-raspberry-pi-clock-weather-display